package Controller;
import Model.*;
import View.Gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controler  {
    private Gui gui;

   private Polinom p=new Polinom();
   private Polinom q=new Polinom();
   private Polinom r=new Polinom();
   
   private Operatii operatii = new Operatii();
   
   private String poli1;
   private String poli2;
    public Controler(Gui gui) {
        this.gui = gui;
        gui.addActionListener(new OpListener());
    }
   private class OpListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
        	poli1=gui.getPoli1();
        	poli2=gui.getPoli2();
        	p.createPol(poli1);
        	q.createPol(poli2);
            	switch(e.getActionCommand()) {
            		case "adunare": operatii.sumPol(p.getPolinom(), q.getPolinom());
            						r = new Polinom(operatii.getResult());
            						String r1 = r.getString();
            						gui.setResult(r1);
            		break;
            		
            		case "scadere":operatii.difPol(p.getPolinom(), q.getPolinom());
									r = new Polinom(operatii.getResult());
									String r2 = r.getString();
									gui.setResult(r2);
            			break;
            			
            		case "inmultire":operatii.prodPol(p.getPolinom(), q.getPolinom());
            						r = new Polinom(operatii.getResult());
            						String r3 = r.getString();
            						gui.setResult(r3);
            			break;
            			
            		case "impartire":
            			break;
            			
            		case "derivare":operatii.derivatePol(p.getPolinom());
									r = new Polinom(operatii.getResult());
									String r4 = r.getString();
									gui.setResult(r4);
            			break;
            			
            		case "integrare":operatii.integratePol(p.getPolinom());
									r = new Polinom(operatii.getResult());
									String r5 = r.getString();
									gui.setResult(r5);
								
            			break;
            			
            		case "stergere": operatii.stergere();
            						gui.setResult("");
            						gui.clearPoliFields();  
            						p.clear();
            						q.clear();
            						r.clear();
            			break;
    
            	}

        }
    }

}
