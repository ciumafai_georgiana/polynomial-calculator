package View;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Gui extends JFrame {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame f = new JFrame("Calculator Polinoame");
    private JPanel panel = new JPanel();
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();
    private JLabel label1 = new JLabel("P(X)");// eticheta polinom1
    private JLabel label2 = new JLabel("Q(X)");// eticheta polinom2
    private JLabel label3 = new JLabel("R(X)");// eticheta rezultat
    private JButton adunare = new JButton("+");
    private JButton scadere = new JButton("-");
    private JButton inmultire = new JButton("*");
    private JButton impartire = new JButton("/");
    private JButton derivare = new JButton("Derivare");
    private JButton integrare = new JButton("Integrare");
    private JButton stergere = new JButton("Clear");
    private JTextField poli1 = new JTextField(50);
    private JTextField poli2 = new JTextField(50);
    private JTextField polirez = new JTextField(40);

    public Gui() {
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(520, 500);
        f.add(panel);
        panel.add(panel1);
        panel.add(panel2);
        panel.add(panel3);
        panel1.setLayout(new FlowLayout());
        panel1.add(label1);
        panel1.add(poli1);
        panel1.add(label2);
        panel1.add(poli2);
        panel2.setLayout(new FlowLayout());
        panel2.add(adunare);
        panel2.add(scadere);
        panel2.add(inmultire);
        panel2.add(impartire);
        panel2.add(derivare);
        panel2.add(integrare);
        panel2.add(stergere);
        panel3.add(label3);
        panel3.add(polirez);
        panel1.setBackground(Color.PINK);
        panel2.setBackground(Color.PINK);
        panel3.setBackground(Color.PINK);
        f.setVisible(true);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    }

    public String getPoli1() {
        return poli1.getText();
    }

    public String getPoli2() {
        return poli2.getText();
    }

    public void setResult(String text) {
    	polirez.setText(text);
    	
    }
    public void clearPoliFields() {
    	poli1.setText("");
    	poli2.setText("");  	
    }

    public void addActionListener(ActionListener e) {
        adunare.addActionListener(e);
        adunare.setActionCommand("adunare");
        scadere.addActionListener(e);
        scadere.setActionCommand("scadere");
        inmultire.addActionListener(e);
        inmultire.setActionCommand("inmultire");
        impartire.addActionListener(e);
        impartire.setActionCommand("impartire");
        derivare.addActionListener(e);
        derivare.setActionCommand("derivare");
        integrare.addActionListener(e);
        integrare.setActionCommand("integrare");
        stergere.addActionListener(e);
        stergere.setActionCommand("stergere");
    }
}
