package Model;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
    ArrayList<Monom> polinom = new ArrayList<Monom>();
    public Polinom()
    {

    }
    public Polinom(ArrayList<Monom> polinom) {
        this.polinom = polinom;
    }
    public ArrayList<Monom> getPolinom() {
        return polinom;
    }

    public void setPolinom(ArrayList<Monom> polinom) {
        this.polinom = polinom;
    }

 
    public void clear() {
    	polinom.clear();
    }
    
    public String getString(){
		String str="";
		int i=0;
		for(Monom m: polinom){
			if(i!=0){
				int ok=1;
				str+=m.toString(ok);
			}
			else{
				int ok=0;
				str+=m.toString(ok);
				i++;
			}
		}
		if(str.length()==0){
			return str + "0";
		}
		return str;
	}
    
    public void createPol(String poly) {
		poly = poly.replaceAll("\\s", "");
		Pattern pattern = Pattern.compile("([+-]?\\d*)x(\\^([+-]?\\d*))?|([+-]\\d+)");
		Matcher matcher = pattern.matcher(poly);	
		while(matcher.find()){ 
			if(matcher.group().length() != 0){	
				int coeficient = 1; int putere = 1;
				if(matcher.group(1) != null){
					coeficient=Integer.parseInt(matcher.group(1));
				}	
				if(matcher.group(3) != null){	
					putere=Integer.parseInt(matcher.group(3));
				}
				if(matcher.group(4) != null){
					coeficient=Integer.parseInt(matcher.group(4));
					putere=0;
				}
				polinom.add(new Monom(coeficient,putere));
			}
		}
	}
}
