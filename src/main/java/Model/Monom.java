package Model;

public class Monom implements Comparable<Monom> {
    private double coeficient;
    private int putere;

    public Monom(double coeficient, int putere) {
        this.coeficient = coeficient;
        this.putere = putere;
    }

    public double getCoeficient() {
        return coeficient;
    }

    public void setCoeficient(double coeficient) {
        this.coeficient = coeficient;
    }

    public int getPutere() {
        return putere;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }

    public String afisareMonom() {
    	if(putere!=0) {
    		return getCoeficient() + "x^" + getPutere();
    	}
    	return getCoeficient() + "";
    }
    
    public String toString(int ok){
		String s="";
		String cs ="" + (double)coeficient;
		if(coeficient==1){
			cs="";
		}
		if(putere==0) {
			if(coeficient>0)
				s+="+"+cs;
			if(coeficient<0)
				s+=cs;
		}
		if(putere != 0){
			if(ok == 1){
				if(coeficient>=0){
					cs="+"+(double)coeficient;
				}
			}
			if(coeficient != 0){
				if(putere!=1){
					s+= cs + "x" + "^" + putere;
				}else{
					s+=cs + "x";
				}
			}else{
				s+="";
			}
		}
		return s;
	}
	@Override
	public int compareTo(Monom m) {
		 int put=((Monom)m).getPutere(); 
	     return put-this.putere;
	}
}
