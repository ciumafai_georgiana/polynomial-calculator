package Model;

import java.util.ArrayList;

public class Operatii {
	 ArrayList<Monom> polinom1 = new ArrayList<Monom>();
	 ArrayList<Monom> polinom2 = new ArrayList<Monom>();
	 ArrayList<Monom> result = new ArrayList<Monom>();

	 
	 public void sumPol(ArrayList<Monom> polinom1,ArrayList<Monom> polinom2){
			this.polinom1 = polinom1;
			this.polinom2 = polinom2;
			int ok = 0;
			//daca puterea monomului din pol1 este egala cu cea a monomului din pol2 se aduna coeficientii si puterea ramane la fel
			//si apoi se adauga in result
			//se sterg monoamele care au avut aceeasi putere si ce ramane in polinoame in primul si al doilea se adauga in result
			for(int i=0;i<polinom1.size();i++){
				for(int j=0;j<polinom2.size();j++){
					if(polinom1.get(i).getPutere()== polinom2.get(j).getPutere()){
						this.result.add(new Monom(polinom1.get(i).getCoeficient() + 
						polinom2.get(j).getCoeficient(),polinom1.get(i).getPutere()));
						polinom2.remove(j);
						j--;
						ok=1;
					}
				}
				if(ok == 1) {
					polinom1.remove(i);
					i--;
					ok=0;
				}
			}
			for(int j=0;j<polinom1.size();j++){
				this.result.add(polinom1.get(j));
			}
			for(int j=0;j<polinom2.size();j++){
				this.result.add(polinom2.get(j));
			}
			sortRes();
	
	}
	 public void difPol(ArrayList<Monom> polinom1,ArrayList<Monom> polinom2){
			this.polinom1 = polinom1;
			this.polinom2 = polinom2;
			int ok =0;
			//daca puterea monomului din pol1 este egala cu cea a monomului din pol2 se scad coeficientii si puterea ramane la fel
			//si apoi se adauga in result
			//se sterg monoamele care au avut aceeasi putere si ce ramane in polinoame in primul si al doilea se adauga in result
			for(int i=0;i<polinom1.size();i++){
				for(int j=0;j<polinom2.size();j++){
					if(polinom1.get(i).getPutere()== polinom2.get(j).getPutere()){
						this.result.add(new Monom(polinom1.get(i).getCoeficient() - 
						polinom2.get(j).getCoeficient(),polinom1.get(i).getPutere()));
						polinom2.remove(j);
						j--;
						ok=1;
					}
				}
				if(ok == 1) {
					polinom1.remove(i);
					i--;
					ok=0;
				}
			}
			for(int j=0;j<polinom1.size();j++){
				this.result.add(polinom1.get(j));
			}
			for(int j=0;j<polinom2.size();j++){
				this.result.add(new Monom(-1*polinom2.get(j).getCoeficient(),polinom2.get(j).getPutere()));
			}
			sortRes();
	 }
	 
	 public void prodPol(ArrayList<Monom> polinom1,ArrayList<Monom> polinom2){
			this.polinom1 = polinom1;
			this.polinom2 = polinom2;
			for(int i=0;i<polinom1.size();i++){
				for(int j=0;j<polinom2.size();j++){
					//se inmultesc coeficientii fiecarui monom din primul polinom cu fiecare monom din al doilea polinom si se aduna puterile lor
					//si apoi se adauga in result
						this.result.add(new Monom(polinom1.get(i).getCoeficient() *polinom2.get(j).getCoeficient(),polinom1.get(i).getPutere() + polinom2.get(j).getPutere()));	
				}
			}
			sortRes();
	}
	
	 private void sortRes() {
		//daca raman monoame cu aceeasi putere se aduna coeficientii lor 
		 for(int i=0;i<result.size()-1;i++) {
			for(int j=i+1;j<result.size();j++) {
				if(result.get(i).getPutere()== result.get(j).getPutere()) {
					result.set(i, new Monom(result.get(i).getCoeficient() + result.get(j).getCoeficient(),result.get(i).getPutere()));
					result.remove(j);
					j--;
				}
			}
			
		 }
		 
		 for(int i=0;i<result.size()-1;i++){
				for(int j=i+1;j<result.size();j++){
					if(result.get(i).getPutere()<result.get(j).getPutere()){
						Monom aux=result.get(i);
						result.set(i, result.get(j));
						result.set(j, aux);
					}
				}
			}
	 }
	 public void derivatePol(ArrayList<Monom> polinom1)
	 {
		 //daca puterea unui monom este 0 atunci nu il punem in rezultat
		 for(int i=0;i<polinom1.size();i++) {
			 if(polinom1.get(i).getPutere()==0) {
				 polinom1.remove(i);
				 i--;
			 }
			 else
			 {
				 //daca puterea unui monom este diferita de 0 atunci se inmulteste coeficientul cu puterea si puterea scade
				 result.add(new Monom(polinom1.get(i).getPutere()*polinom1.get(i).getCoeficient(),polinom1.get(i).getPutere()-1));
			 }
				 
		 }
	 }
	 public void integratePol(ArrayList<Monom> polinom1)
	 {
		 for(int i=0;i<polinom1.size();i++) {
			 	int put=polinom1.get(i).getPutere()+1;
				 result.add(new Monom(polinom1.get(i).getCoeficient()/put,put)); 
		 }
		// sortRes();
		 
	 }
	 
	 public void stergere()
	 {
		 this.polinom1.clear();
		this.polinom2.clear();
		this.result.clear();
	 }
	
	public ArrayList<Monom> getResult() {
		return this.result;
	}
}
