import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import Model.*;

import org.junit.Assert;
import org.junit.Test;

public class Junit {
	
	 ArrayList<Monom> polinom1 = new ArrayList<Monom>();
	 ArrayList<Monom> polinom2 = new ArrayList<Monom>();
	 ArrayList<Monom> result = new ArrayList<Monom>();
	 
	 ArrayList<Monom> rezAdd=new ArrayList<Monom>();
	 ArrayList<Monom> rezDif=new ArrayList<Monom>();
	 ArrayList<Monom> rezProd=new ArrayList<Monom>();
	 ArrayList<Monom> rezDiv=new ArrayList<Monom>();
	 ArrayList<Monom>rezDer=new ArrayList<Monom>();
	 ArrayList<Monom>rezInt=new ArrayList<Monom>();
	
	 void verify(Polinom result, Polinom result2){
	   Collections.sort(result.getPolinom());
	   Iterator<Monom> i1=result.getPolinom().iterator();
	   Iterator<Monom> i2= result2.getPolinom().iterator();
	   while (i1.hasNext() && i2.hasNext()) {
	      Monom m = (Monom) i1.next();
	      Monom m2 = (Monom) i2.next();
	      assertEquals(m.getCoeficient(), m2.getCoeficient(),0);
	      assertEquals(m.getPutere(), m2.getPutere(),0);
	        }
	   }
	 
	public void setUp() {

	polinom1.add(new Monom(4,2));
	polinom1.add(new Monom(2,0));

	polinom2.add(new Monom(2,2));
	polinom2.add(new Monom(2,0));
	
	rezAdd.add(new Monom(6,2));
	rezAdd.add(new Monom(4,0));
	
	rezDif.add(new Monom(2,2));
	
	rezProd.add(new Monom(8,4));
	rezProd.add(new Monom(12,2));
	rezProd.add(new Monom(4,0));
	
	rezDer.add(new Monom(8,1));
	
	rezInt.add(new Monom(1.333333333333333,1));
	rezInt.add(new Monom(2,1));
	}
	
	@Test
	public void testAdd() {
		setUp();
		Operatii o=new Operatii();
		o.sumPol(polinom1,polinom2);
		Polinom r = new Polinom(o.getResult());
		Polinom rAdd=new Polinom(rezAdd);
		verify(r,rAdd);
	}
	@Test
	public void TestDif() {
		setUp();
		Operatii o=new Operatii();
		o.difPol(polinom1,polinom2);
		Polinom r = new Polinom(o.getResult());
		Polinom rDif=new Polinom(rezDif);
		verify(r,rDif);
		
	}
	@Test
	public void TestProd() {
		setUp();
		Operatii o=new Operatii();
		o.prodPol(polinom1,polinom2);
		Polinom r = new Polinom(o.getResult());
		Polinom rProd=new Polinom(rezProd);
		verify(r,rProd);
	}
	@Test
	public void TestDerivate() {
		setUp();
		Operatii o=new Operatii();
		o.derivatePol(polinom1);
		Polinom r=new Polinom(o.getResult());
		Polinom rezD=new Polinom(rezDer);
		verify(r,rezD);
	}
	@Test
	public void TestIntegrate() {
		polinom1.add(new Monom(6,2));
		polinom1.add(new Monom(2,0));
		Operatii o=new Operatii();
		o.derivatePol(polinom1);
		Polinom r=new Polinom(o.getResult());
		Polinom rezI=new Polinom(rezInt);
		verify(r,rezI);
	}
}
